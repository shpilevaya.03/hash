﻿namespace Project
{
    public enum HashFunction
    {
        DivideMethod,
        MultiplyMethod,
        FNVMethod
    }
}