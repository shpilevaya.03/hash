﻿using System.Security.Cryptography;
using System.Text;

namespace Project
{
    class ChainHashTable : HashTable
    {
        public ChainHashTable(int size)
        {
            this.size = size;
            items = new Dictionary<int, List<Item>>(size);
            count = 0;
        }

        private Dictionary<int, List<Item>> items = null;
        private HashFunction hashFunction;

        private int count { get; set; }
        private readonly int size;
        public double FillFactor => (double)count / size;

        public List<int> ChainLengths => items.Where(x => x.Value != null).Select(x => x.Value.Count).ToList();
        public int MaxLengthChain => items.Where(x => x.Value != null).Max(x => x.Value.Count);
        public int MinLengthChain => items.Where(x => x.Value != null).Min(x => x.Value.Count);

        public override string Search(int key)
        {
            int hash = GetHash(key);
            if (!items.ContainsKey(hash))
            {
                return null;
            }

            List<Item> hashtableItems = items[hash];
            if (hashtableItems != null)
            {
                Item? item = hashtableItems.SingleOrDefault(i => i.Key == key);
                if (item != null)
                {
                    return item.Value;
                }
            }
            return null;
        }

        public override void Add(Item item)
        {
            int hash = GetHash(item.Key);
            List<Item> hashTableItem = null;

            if (items.ContainsKey(hash))
            {
                hashTableItem = items[hash];
                items[hash].Add(item);
            }
            else
            {
                hashTableItem = new List<Item> { item };
                items.Add(hash, hashTableItem);
            }
            count++;
        }

        public override void Remove(int key)
        {
            int hash = GetHash(key);
            if (!items.ContainsKey(hash))
            {
                return;
            }

            List<Item> hashtableItems = items[hash];
            Item? item = hashtableItems.SingleOrDefault(i => i.Key == key);

            if (item != null)
            {
                hashtableItems.Remove(item);
            }
            count--;
        }

        public void SetHashFunction()
        {
            List<string> options = new()
            {
                "Метод деления",
                "Метод умножения",
                "FVN хеш-функция"
            };

            Menu functionChoice = new(options);
            functionChoice.DrawMenu();

            switch (functionChoice.Run())
            {
                case 0:
                    hashFunction = HashFunction.DivideMethod;
                    return;
                case 1:
                    hashFunction = HashFunction.MultiplyMethod;
                    return;
                case 2:
                    hashFunction = HashFunction.FNVMethod;
                    return;
                default:
                    hashFunction = HashFunction.DivideMethod;
                    return;
            }
        }

        private int GetHash(int key)
        {
            return hashFunction switch
            {
                HashFunction.DivideMethod => GetHashDivideMethod(key, size),
                HashFunction.MultiplyMethod => GetHashMultiplyMethod(key, size),
                HashFunction.FNVMethod => GetHashFNVMethod(key, size)
            };
        }
    }
}