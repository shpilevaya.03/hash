﻿namespace Project
{
    class OpenAddressHashTable : HashTable
    {
        public OpenAddressHashTable(int size)
        {
            this.size = size;
            items = new Item[size];
        }

        private Item[] items = null;
        private HashFunction hashFunction;
        private ProbingMethod probingMethod;

        private readonly int size;

        private Dictionary<int, int> clusters = new();

        public override string Search(int key)
        {
            int index = 0;
            int hash = GetProbing(key, index);

            while (index < size)
            {
                if (items[hash].Key == key)
                {
                    return items[hash].Value;
                }
                index++;
                hash = GetProbing(key, index);
            }

            return null;
        }

        public override void Add(Item item)
        {
            int index = 0;
            int hash = GetProbing(item.Key, index);

            while (items[hash] != null && !items[hash].Key.Equals(item.Key))
            {
                index++;
                hash = GetProbing(item.Key, index);
            }
            items[hash] = item;
            clusters.Add(hash, index);
        }

        public override void Remove(int key)
        {
            int index = 0;
            int hash = GetProbing(key, index);

            while (items[hash] != null && !items[hash].Key.Equals(key))
            {
                if (items[hash].Key == key)
                {
                    items[hash] = null;
                    break;
                }
                index++;
                hash = GetProbing(key, index);
            }
        }

        public int MaxClusterLength() => clusters.Values.Max();

        public void SetProbingMethod()
        {
            List<string> options = new()
            {
                "Линейное исследование",
                "Квадратичное исследование",
                "Двойное хеширование"
            };

            Console.WriteLine("Выберите хеш-функцию:");
            Menu functionChoice = new(options);
            functionChoice.DrawMenu();

            switch (functionChoice.Run())
            {
                case 0:
                    probingMethod = ProbingMethod.Linear;
                    return;
                case 1:
                    probingMethod = ProbingMethod.Quadratic;
                    return;
                case 2:
                    probingMethod = ProbingMethod.Double;
                    return;
                default:
                    probingMethod = ProbingMethod.Linear;
                    return;
            }
        }

        private int GetProbing(int key, int index)
        {
            return probingMethod switch
            {
                ProbingMethod.Linear => LinearHashing(GetHash(hashFunction), key, size, index),
                ProbingMethod.Quadratic => QuadraticHashing(GetHash(hashFunction), key, size, index),
                ProbingMethod.Double => DoubleHashing(GetHash(hashFunction), GetHash(hashFunction), key, size, index)
            };
        }

        public void SetHashFunction()
        {
            Console.WriteLine("Выберите хеш-функцию (Нажмите Enter чтобы выбрать):");
            List<string> options = new()
            {
                "Метод деления",
                "Метод умножения",
                "FVN хеш-функция"
            };

            Menu functionChoice = new(options);
            functionChoice.DrawMenu();

            switch (functionChoice.Run())
            {
                case 0:
                    hashFunction = HashFunction.DivideMethod;
                    return;
                case 1:
                    hashFunction = HashFunction.MultiplyMethod;
                    return;
                case 2:
                    hashFunction = HashFunction.FNVMethod;
                    return;
                default:
                    hashFunction = HashFunction.DivideMethod;
                    return;
            }
        }

        private Func<int, int, int> GetHash(HashFunction hashFunction)
        {
            return hashFunction switch
            {
                HashFunction.DivideMethod => GetHashDivideMethod,
                HashFunction.MultiplyMethod => GetHashMultiplyMethod,
                HashFunction.FNVMethod => GetHashFNVMethod
            };
        }


        public Func<Func<int, int, int>, int, int, int, int> LinearHashing = (function, key, size, index) =>
        {
            return (function(key, size) + index) % size;
        };

        public Func<Func<int, int, int>, int, int, int, int> QuadraticHashing = (function, key, size, index) =>
        {
            return (int)Math.Abs((function(key, size) + Math.Pow(index, 2)) % size);
        };

        public Func<Func<int, int, int>, Func<int, int, int>, int, int, int, int> DoubleHashing = (function1, function2, key, size, index) =>
        {
            return (int)Math.Abs((function1(key, size) + index * function2(key, size)) % size);
        };
    }
}