﻿using System.Collections.Generic;

namespace Project
{
    class Program
    {
        private static readonly string lengthsPath = @"lengths.csv";
        private static readonly string clustersPath = @"clusters.csv";

        private static void Main()
        {
            List<string> options = new()
            {
                "Создать хеш-таблицу, использующую метод разрешения коллизий с помощью цепочек",
                "Создать хеш-таблицу, использующую метод открытой адресации для разрешения коллизий"
            };

            Menu mainMenu = new(options);
            mainMenu.DrawMenu();

            switch (mainMenu.Run())
            {
                case 0:
                    ChainHashTable chainHashTable = new(1000);
                    chainHashTable.SetHashFunction();

                    foreach (Item item in GenerateData(100000))
                    {
                        chainHashTable.Add(item);
                    }

                    File.WriteAllText(lengthsPath, String.Empty);
                    int index = 1;
                    foreach (int length in chainHashTable.ChainLengths)
                    {
                        File.AppendAllText(lengthsPath, $"{index}, {length}\n");
                        index++;
                    }
                    break;

                case 1:
                    OpenAddressHashTable openAddressHashTable = new(10000);
                    openAddressHashTable.SetHashFunction();
                    openAddressHashTable.SetProbingMethod();

                    foreach (Item item in GenerateData(10000))
                    {
                        openAddressHashTable.Add(item);
                    }

                    File.WriteAllText(clustersPath, $"{openAddressHashTable.MaxClusterLength()}");
                    break;
            }  
        }

        public static List<Item> GenerateData(int amount)
        {
            int[] start = Enumerable.Range(0, amount).ToArray();
            List<Item> items = new();
            Random random = new();

            List<int> keys = new();
            List<string> values = new();

            for (var i = 0; i < amount; i++)
                keys.Add(start[i] + random.Next() + i);

            for (var j = 0; j < amount; j++)
            {
                var s = "";
                for (var i = 0; i < 5; i++)
                {
                    var a = (char)random.Next(0, 255);
                    s += a;
                }
                values.Add(s);
            }

            for (int k = 0; k < amount; k++)
                items.Add(new Item(keys[k], values[k]));

            return items;
        }
    }
}