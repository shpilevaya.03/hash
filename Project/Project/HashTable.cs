﻿namespace Project
{
    public abstract class HashTable
    {
        public abstract string Search(int key);
        public abstract void Add(Item item);
        public abstract void Remove(int key);

        public int GetHashDivideMethod(int key, int size) => Math.Abs(key.GetHashCode() % size);
        public int GetHashMultiplyMethod(int key, int size) => (int)Math.Abs(size * (key.GetHashCode() * ((1 + Math.Sqrt(5)) / 2) % 1));
        public int GetHashFNVMethod(int key, int size)
        {
            uint fnv_prime = 0x811C9DC5;
            uint hash = 0;
            uint i = 0;
            string str = key.ToString();

            for (i = 0; i < str.Length; i++)
            {
                hash *= fnv_prime;
                hash ^= ((byte)str[(int)i]);
            }
            return (int)Math.Abs(hash % size);
        }
    }
}