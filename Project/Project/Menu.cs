﻿using static System.Console;

namespace Project
{
    class Menu
    {
        private int SelectedIndex;
        private List<string> Options;

        public Menu(List<string> options)
        {
            Options = options;
            SelectedIndex = 0;
        }

        public void DrawMenu()
        {
            string suffix;
            for (int i = 0; i < Options.Count; i++)
            {
                string currentOption = Options[i];
                if (i == SelectedIndex)
                {
                    suffix = "<";
                }
                else
                {
                    suffix = " ";
                }
                WriteLine($"{currentOption} {suffix}");
            }
        }

        public int Run()
        {
            ConsoleKey keyPressed;
            do
            {
                Clear();
                DrawMenu();
                ConsoleKeyInfo keyInfo = ReadKey(true);
                keyPressed = keyInfo.Key;
                if (keyPressed == ConsoleKey.UpArrow)
                {
                    SelectedIndex--;
                    if (SelectedIndex == -1) SelectedIndex = 0;
                }
                if (keyPressed == ConsoleKey.DownArrow)
                {
                    SelectedIndex++;
                    if (SelectedIndex == Options.Count) SelectedIndex = Options.Count - 1;
                }

            } while (keyPressed != ConsoleKey.Enter);
            return SelectedIndex;
        }
    }
}